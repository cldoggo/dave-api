const express = require('express');
const mysql = require('mysql');
const pool  = mysql.createPool({
  connectionLimit : 3,
  host            : 'localhost',
  user            : 'dave_admin',
  password        : 'testing123',
  database        : 'users'
});
const bluebird = require('bluebird');
bluebird.promisifyAll(pool);
const routes = require('./routes')(pool);


const app = express();

// Routes
// POST /activity
app.post('/activity', routes['activity']['post']);
// GET /stats
app.get('/stats', routes['stats']['get']);

// Raise app instance
app.listen(8080, () =>{
  console.info('API raised on http://localhost:8080')
});

