const moment = require('moment');
const Promise = require('bluebird');

/**
* Saves a user_activity record
* @param {Object} mySqlConnectionPool - https://github.com/mysqljs/mysql#pooling-connections
* @param {Object} data
* @param {String} data.user_id
* @param {String} data.session_id
* @param {Number} retry (ms) - retry / exponential backoff. Max attempts: 10 
*/
function save(mySqlConnectionPool, data, retry=100){
  // ts: current timestamp from Unix epoch in ms
  const timeStampedData = Object.assign({}, data, { ts: moment().valueOf() } );
  // NodeJS MySql driver favors '?' var formatting but string literals would be my own preferred implementation
  const query = `INSERT INTO users.user_activity SET ?`;
  return mySqlConnectionPool.queryAsync(query, timeStampedData)
    .catch((error) => {
    if (retry < 102400){
      return save(mySqlConnectionPool, data, retry*2)
    }
    return error;
  });
}

/**
* Gets a user_activity record
* @param {Object} mySqlConnectionPool - https://github.com/mysqljs/mysql#pooling-connections
* @param {Object} params
* @param {String} params.start_date YYYY-MM-DD (inclusive)
* @param {String} params.end_date YYYY-MM-DD (inclusive)
* @param {String} params.user_id - filter response to a single user
* @returns (ideally) results + pagination token
*/
function get(mySqlConnectionPool, params, retry=100){
  const start = moment(params.start_date).valueOf();
  const end = moment(params.end_date).valueOf();
  // NodeJS MySql driver favors '?' var formatting but string literals would be my own preferred implementation
  if (params.user_id){
    const contraints = [start, end, params.user_id];
    const distinct_users_query = 'SELECT count(distinct(user_id)) from users.user_activity WHERE ts BETWEEN ? and ? AND user_id = ?';
    const session_count_query = 'SELECT count(*) from users.user_activity WHERE ts BETWEEN ? and ? AND user_id = ? GROUP BY user_id';
    const avg_sessions_per_user_query = 'SELECT count(distinct(session_id)) as session_count from users.user_activity WHERE ts BETWEEN ? and ? and user_id = ? GROUP_BY user_id';
  } else {
    const constraints = [start, end];
    const distinct_users_query = 'SELECT count(distinct(user_id)) from users.user_activity WHERE ts BETWEEN ? and ?';
    const session_count_query = 'SELECT count(*) from users.user_activity WHERE ts BETWEEN ? and ? GROUP BY user_id';
    const avg_sessions_per_user_query = 'SELECT count(distinct(session_id)) as session_count from users.user_activity WHERE ts BETWEEN ? and ? GROUP_BY user_id';
  }

  return Promise.all([
    // requirement: unique_users (integer): number of unique users in the given time period
    mySqlConnectionPool.queryAsync(distinct_query),
    // requirement: num_sessions (integer): number of sessions in the given time period (or all time if no period is passed)
    mySqlConnectionPool.queryAsync(session_count_query),
    // requirement avg_sessions_per_user arithmatic mean number of sessions per user
    mySqlConnectionPool.queryAsync(avg_sessions_per_user_query)
    ]).then(([distinctUsers, sessionCount, avgSessions]) =>{
      return {
        unique_users: distinctUsers,
        num_sessions: sessionCount,
        avg_sessions_per_user: avgSessions
      };
    });
}

function activity(mySqlConnectionPool, req, res) {
  const { user_id, session_id } = req.body;
  if (!user_id || session_id){
    res.status(400).send('user_id <string> and session_id <string> are both required.')
  }
  return save(mySqlConnectionPool, req.body);
}

function stats(mySqlConnectionPool, req, res){
  const { start_date, end_date, user_id } = req.params;
  if (!start_date || !end_date){
    return res.status(400).send('start_date <string> and end_date <string> are required in format YYYY-MM-DD');
  }
  return get(mySqlConnectionPool, {start_date, end_date, user_id})
  .then(results => {
    return res.status(200).send(results);
  });
}


module.exports = (mySqlConnectionPool) => {
  return {
    stats: {
      get: (req, res) => stats(mySqlConnectionPool, req, res)
    },
    activity: {
      post: (req, res) => activity(mySqlConnectionPool, req, res)
    }
  };
}